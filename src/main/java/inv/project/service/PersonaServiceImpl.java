package inv.project.service;

import inv.project.dao.PersonaDao;
import inv.project.model.Persona;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDao personaDao;

	@Transactional
	public Integer save(Persona persona) {
		return personaDao.save(persona);
	}

	public Persona get(Integer id) {
		return personaDao.get(id);
	}

	public List<Persona> list() {
		return personaDao.list();
	}

	@Transactional
	public void update(Integer id, Persona persona) {
		personaDao.update(id, persona);
	}

	@Transactional
	public void delete(Integer id) {
		personaDao.delete(id);
	}

}
