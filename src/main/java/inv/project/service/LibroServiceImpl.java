package inv.project.service;

import inv.project.dao.LibroDao;
import inv.project.model.Libro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class LibroServiceImpl implements LibroService {

	@Autowired
	private LibroDao libroDao;

	@Transactional
	public Integer save(Libro libro) {
		return libroDao.save(libro);
	}

	public Libro get(Integer id) {
		return libroDao.get(id);
	}

	public List<Libro> list() {
		return libroDao.list();
	}

	@Transactional
	public void update(Integer id, Libro libro) {
		libroDao.update(id, libro);
	}

	@Transactional
	public void delete(Integer id) {
		libroDao.delete(id);
	}
	
}
