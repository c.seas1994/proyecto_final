package inv.project.service;

import inv.project.model.Libro;

import java.util.List;

public interface LibroService {

	Integer save(Libro libro);
	Libro get(Integer id);
	List<Libro> list();
	void update(Integer id, Libro libro);
	void delete(Integer id);
}
