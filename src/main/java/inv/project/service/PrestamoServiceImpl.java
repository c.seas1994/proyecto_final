package inv.project.service;

import inv.project.dao.PrestamoDao;
import inv.project.model.Prestamo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class PrestamoServiceImpl implements PrestamoService{
	
	@Autowired
	private PrestamoDao prestamoDao;

	@Transactional
	public Integer save(Prestamo prestamo) {
		return prestamoDao.save(prestamo);
	}

	public Prestamo get(Integer id) {
		return prestamoDao.get(id);
	}

	public List<Prestamo> list() {
		return prestamoDao.list();
	}

	@Transactional
	public void update(Integer id, Prestamo prestamo) {
		prestamoDao.update(id, prestamo);
	}

	@Transactional
	public void delete(Integer id) {
		prestamoDao.delete(id);
	}
}
