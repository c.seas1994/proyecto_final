package inv.project.controller;

import inv.project.model.Persona;
import inv.project.service.PersonaService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PersonaController {

	@Autowired
	private PersonaService personaService;

	//Agregar nueva persona
	@PostMapping("/persona")
	public ResponseEntity<?> save(@RequestBody Persona persona) {
		long id = personaService.save(persona);
		return ResponseEntity.ok().body("New Book has been saved with ID:" + id);
	}
	
	// Obtener persona por id
	@GetMapping("/personas")
	public ResponseEntity<List<Persona>> getAll() {
		List<Persona> personas = personaService.list();
		return ResponseEntity.ok().body(personas);
	}
	
	//Obtener persona por id
	@GetMapping("/persona/{id}")
	public ResponseEntity<Persona> getById(@PathVariable("id") int id) {
		Persona persona = personaService.get(id);
		return ResponseEntity.ok().body(persona);
	}
	
	//Actualizar persona por id
	@PutMapping("/persona/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Persona persona){
		personaService.update(id, persona);
		return ResponseEntity.ok().body("Persona ha sido actualizado exitosamente.");
	}
	
	//Eliminar persona por id
   @DeleteMapping("/persona/{id}")
   public ResponseEntity<?> delete(@PathVariable("id") int id) {
      personaService.delete(id);
      return ResponseEntity.ok().body("Persona ha sido borrada exitosamente.");
   }
	
}
