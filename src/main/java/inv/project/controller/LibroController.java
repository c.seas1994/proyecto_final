package inv.project.controller;

import inv.project.model.Libro;
import inv.project.service.LibroService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class LibroController {
	
	@Autowired
	private LibroService libroService;

	//Agregar un nuevo Libro
	@PostMapping("/libro")
	public ResponseEntity<?> save(@RequestBody Libro libro) {
		long id = libroService.save(libro);
		return ResponseEntity.ok().body("Nuevo libro ha sido creado con el ID:" + id);
	}
	
	// Obtener persona por id
	@GetMapping("/libros")
	public ResponseEntity<List<Libro>> getAll() {
		List<Libro> libros = libroService.list();
		return ResponseEntity.ok().body(libros);
	}

	//Obtener un libro por ID
	@GetMapping("/libro/{id}")
	public ResponseEntity<Libro> get(@PathVariable("id") int id) {
		Libro libro = libroService.get(id);
		return ResponseEntity.ok().body(libro);
	}
	
	//Actualizar un libro por ID
	@PutMapping("/libro/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Libro libro){
		libroService.update(id, libro);
		return ResponseEntity.ok().body("Libro ha sido actualizado exitosamente.");
	}
	
	// Eliminar un libro por ID
   @DeleteMapping("/libro/{id}")
   public ResponseEntity<?> delete(@PathVariable("id") int id) {
      libroService.delete(id);
      return ResponseEntity.ok().body("Libro ha sido borrada exitosamente.");
   }
	
}
