package inv.project.controller;

import inv.project.model.Prestamo;
import inv.project.service.PrestamoService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PrestamoController {

	@Autowired
	private PrestamoService prestamoService;

	//Agregar nueva prestamo
	@PostMapping("/prestamo")
	public ResponseEntity<?> save(@RequestBody Prestamo prestamo) {
		long id = prestamoService.save(prestamo);
		return ResponseEntity.ok().body("New Book has been saved with ID:" + id);
	}
	
	// Obtener prestamos
	@GetMapping("/prestamos")
	public ResponseEntity<List<Prestamo>> getAll(){
		List<Prestamo> prestamos = prestamoService.list();
		return ResponseEntity.ok().body(prestamos);
	}

	//Obtener persona por id
	@GetMapping("/prestamo/{id}")
	public ResponseEntity<Prestamo> get(@PathVariable("id") int id) {
		Prestamo prestamo = prestamoService.get(id);
		return ResponseEntity.ok().body(prestamo);
	}
	
	//Actualizar persona por id
	@PutMapping("/prestamo/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Prestamo prestamo){
		prestamoService.update(id, prestamo);
		return ResponseEntity.ok().body("Prestamo ha sido actualizado exitosamente.");
	}
	
	//Eliminar persona por id
   @DeleteMapping("/prestamo/{id}")
   public ResponseEntity<?> delete(@PathVariable("id") int id) {
	  prestamoService.delete(id);
      return ResponseEntity.ok().body("Prestamo ha sido borrada exitosamente.");
   }
	
}
