package inv.project.dao;

import inv.project.model.Persona;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonaDaoImp implements PersonaDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Persona persona) {
		sessionFactory.getCurrentSession().save(persona);
		return persona.getIdPersona();
	}

	public Persona get(Integer id) {
		return sessionFactory.getCurrentSession().get(Persona.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Persona> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Persona> query = session.createQuery("from Persona");
		List<Persona> personas = query.list();
		return personas;
	}

	public void update(Integer id, Persona persona) {
		Session session = sessionFactory.getCurrentSession();
		Persona personaActualizada = session.byId(Persona.class).load(id);
		personaActualizada.setNombre(persona.getNombre());
		personaActualizada.setApellido(persona.getApellido());
		personaActualizada.setEdad(persona.getEdad());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Persona persona = session.byId(Persona.class).load(id);
		session.delete(persona);
	}

}
