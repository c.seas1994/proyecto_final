package inv.project.dao;

import inv.project.model.Prestamo;

import java.util.List;

public interface PrestamoDao {

	Integer save(Prestamo prestamo);
	
	Prestamo get(Integer id);
	
	List<Prestamo> list();
	
	void update(Integer id, Prestamo prestamo);

	void delete(Integer id);
	
}
