package inv.project.dao;

import inv.project.model.Libro;
import inv.project.model.Persona;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LibroDaoImp implements LibroDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Integer save(Libro libro) {
		sessionFactory.getCurrentSession().save(libro);
		return libro.getIdLibro();
	}
	
	public Libro get(Integer id) {
		return sessionFactory.getCurrentSession().get(Libro.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Libro> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Libro> query = session.createQuery("from Libro");
		List<Libro> libros = query.list();
		return libros;
	}
	
	public void update(Integer id, Libro libro) {
		Session session = sessionFactory.getCurrentSession();
		Libro libroActualizada = session.byId(Libro.class).load(id);
		libroActualizada.setNombre(libro.getNombre());
		libroActualizada.setAutor(libro.getAutor());
		libroActualizada.setEditorial(libro.getEditorial());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Libro libro = session.byId(Libro.class).load(id);
		session.delete(libro);
	}
}
