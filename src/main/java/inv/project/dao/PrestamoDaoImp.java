package inv.project.dao;

import inv.project.model.Prestamo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PrestamoDaoImp implements PrestamoDao{

	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Prestamo prestamo) {
		sessionFactory.getCurrentSession().save(prestamo);
		return prestamo.getIdPrestamo();
	}

	public Prestamo get(Integer id) {
		return sessionFactory.getCurrentSession().get(Prestamo.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Prestamo> list() {
		Session session = sessionFactory.getCurrentSession();
		Query<Prestamo> query = session.createQuery("from Prestamo");
		List<Prestamo> prestamos = query.list();
		return prestamos;
	}

	public void update(Integer id, Prestamo prestamo) {
		Session session = sessionFactory.getCurrentSession();
		Prestamo prestamoActualizada = session.byId(Prestamo.class).load(id);
		prestamoActualizada.setFecha(prestamo.getFecha());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Prestamo prestamo = session.byId(Prestamo.class).load(id);
		session.delete(prestamo);
	}
	
}
