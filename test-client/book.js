$(document).ready(function(){
    $.ajax({
        url: 'http://localhost:8080/libros',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('#table-book');

            tbodyEl.html('');

            response.forEach(function(libro) {
                tbodyEl.append('\
                    <tr>\
                        <td class="idLibro">' + libro.idLibro + '</td>\
                        <td><input type="text" class="nameBook" value="' + libro.nombre + '"></td>\
                        <td><input type="text" class="author" value="' + libro.autor + '"></td>\
                        <td><input type="text" class="editorial" value="' + libro.editorial + '"></td>\
                        <td>\
                            <div class="btn-block">\
                                <button class="btn update-button">Update</button>\
                                <button id="delete-button" class="btn delete">DELETE</button>\
                            </div>\
                        </td>\
                    </tr>\
                ');
            });
        }
    });

});

$(function() {
    // READ/GET
    $('#get-books').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/libros',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#table-book');
    
                tbodyEl.html('');
    
                response.forEach(function(libro) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="idLibro">' + libro.idLibro + '</td>\
                            <td><input type="text" class="nameBook" value="' + libro.nombre + '"></td>\
                            <td><input type="text" class="author" value="' + libro.autor + '"></td>\
                            <td><input type="text" class="editorial" value="' + libro.editorial + '"></td>\
                            <td>\
                                <div class="btn-block">\
                                    <button class="btn update-button">Update</button>\
                                    <button id="delete-button" class="btn delete">DELETE</button>\
                                </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-book').on('submit', function(event) {
        event.preventDefault();

        var info = { 
			nombre: $('#nombreLibro').val(),
			autor: $('#autor').val(),
			editorial: $('#editorial').val(),	
		};

        $.ajax({
            url: 'http://localhost:8080/libro/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nombreLibro').val(''),
				$('#autor').val(''),
				$('#editorial').val(''),
                $('#get-books').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.idLibro').text();
        var newNameBook = rowEl.find('.nameBook').val();
        var newAuthor = rowEl.find('.author').val();
        var newEditorial = rowEl.find('.editorial').val();

        var info = { 
			nombre: newNameBook,
			autor: newAuthor,
			editorial: newEditorial,	
		};

        $.ajax({
            url: 'http://localhost:8080/libro/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-books').click();
            }
        });
    });

    // DELETE
    $('#table-book').on('click', '#delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.idLibro').text();

        $.ajax({
            url: 'http://localhost:8080/libro/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-books').click();
            }
        });
    });
});