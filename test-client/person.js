$(document).ready(function(){

    $.ajax({
        url: 'http://localhost:8080/personas',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('#table-person');

            tbodyEl.html('');

            response.forEach(function(persona) {
                tbodyEl.append('\
                    <tr>\
                        <td class="idPersona">' + persona.idPersona + '</td>\
                        <td><input type="text" class="name" value="' + persona.nombre + '"></td>\
                        <td><input type="text" class="lastName" value="' + persona.apellido + '"></td>\
                        <td><input type="text" class="age" value="' + persona.edad + '"></td>\
                        <td>\
                            <div class="btn-block">\
                                <button class="btn update-button">Update</button>\
                                <button id="delete-button" class="btn delete">DELETE</button>\
                            </div>\
                        </td>\
                    </tr>\
                ');
            });
        }
    });

});

$(function() {
    // READ/GET
    $('#get-persons').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/personas',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#table-person');
    
                tbodyEl.html('');
    
                response.forEach(function(persona) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="idPersona">' + persona.idPersona + '</td>\
                            <td><input type="text" class="name" value="' + persona.nombre + '"></td>\
                            <td><input type="text" class="lastName" value="' + persona.apellido + '"></td>\
                            <td><input type="text" class="age" value="' + persona.edad + '"></td>\
                            <td>\
                                <div class="btn-block">\
                                    <button class="btn update-button">Update</button>\
                                    <button id="delete-button" class="btn delete">DELETE</button>\
                                </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-person').on('submit', function(event) {
        event.preventDefault();

        var info = { 
			nombre: $('#nombre').val(),
			apellido: $('#apellido').val(),
			edad: $('#edad').val(),	
		};

        $.ajax({
            url: 'http://localhost:8080/persona/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nombre').val(''),
				$('#apellido').val(''),
				$('#edad').val(''),
                $('#get-persons').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.idPersona').text();
        var newName = rowEl.find('.name').val();
        var newLastName = rowEl.find('.lastName').val();
        var newAge = rowEl.find('.age').val();

        var info = { 
			nombre: newName,
			apellido: newLastName,
			edad: newAge,	
		};

        $.ajax({
            url: 'http://localhost:8080/persona/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-persons').click();
            }
        });
    });

    // DELETE
    $('#table-person').on('click', '#delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.idPersona').text();

        $.ajax({
            url: 'http://localhost:8080/persona/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-persons').click();
            }
        });
    });
});