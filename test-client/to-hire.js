$(document).ready(function(){

    $.ajax({
        url: 'http://localhost:8080/prestamos',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('#table-to-hire');

            tbodyEl.html('');

            response.forEach(function(prestamo) {
                tbodyEl.append('\
                    <tr>\
                        <td class="id">' + prestamo.idPrestamo + '</td>\
                        <td><input type="text" class="idLibro" value="' + prestamo.libro.nombre + '"></td>\
                        <td><input type="text" class="idPersona" value="' + prestamo.persona.nombre + ' ' + prestamo.persona.apellido + '"></td>\
                        <td><input type="text" class="fecha" value="' + prestamo.fecha + '"></td>\
                        <td>\
                            <div class="btn-block">\
                                <button id="delete-button" class="btn delete">DELETE</button>\
                            </div>\
                        </td>\
                    </tr>\
                ');
            });
        }
    });

    // <button class="btn update-button">Update</button>\

    $.ajax({
        url: 'http://localhost:8080/personas',
        contentType: 'application/json',
        success: function(response) {
            var personaSection = $('#person-op');

            personaSection.html('');

            response.forEach(function(persona) {
                personaSection.append('\
                    <option id="idPersona" value=" ' + persona.idPersona + ' "> ' + persona.nombre + ' ' + persona.apellido + ' </option>\
                ');
            });
        }
    });

    $.ajax({
        url: 'http://localhost:8080/libros',
        contentType: 'application/json',
        success: function(response) {
            var bookSection = $('#book-op');

            bookSection.html('');

            response.forEach(function(libro) {
                bookSection.append('\
                    <option id="idLibro" value=" ' + libro.idLibro + ' "> ' + libro.nombre + ' </option>\
                ');
            });
        }
    });


});

$(function() {

    $('#get-to-hire').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/prestamos',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('#table-to-hire');
    
                tbodyEl.html('');
    
                response.forEach(function(prestamo) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + prestamo.idPrestamo + '</td>\
                            <td><input type="text" class="idLibro" value="' + prestamo.libro.nombre + '"></td>\
                            <td><input type="text" class="idPersona" value="' + prestamo.persona.nombre + ' ' + prestamo.persona.apellido + '"></td>\
                            <td><input type="text" class="fecha" value="' + prestamo.fecha + '"></td>\
                            <td>\
                                <div class="btn-block">\
                                    <button class="btn update-button">Update</button>\
                                    <button id="delete-button" class="btn delete">DELETE</button>\
                                </div>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-to-hire').on('submit', function(event) {
        event.preventDefault();

        var persona = {
            idPersona: $('#person-op option:selected').val()
        };

        var libro = {
            idLibro: $('#book-op option:selected').val()
        };

        var info = { 
            persona: persona,
            libro: libro,
			fecha: $('#fecha').val()
		};

        $.ajax({
            url: 'http://localhost:8080/prestamo/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#idLibro').val(''),
				$('#idPersona').val(''),
				$('#fecha').val(''),
                $('#get-to-hire').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newHireBook = rowEl.find('.idLibro').val();
        var newHirePerson = rowEl.find('.idPersona').val();
        var newFecha = rowEl.find('.fecha').val();

        var info = { 
			idLibro: $('#idLibro').val(),
			idPersona: $('#idPersona').val(),
			fecha: $('#fecha').val(),	
		};

        $.ajax({
            url: 'http://localhost:8080/prestamo/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-to-hire').click();
            }
        });
    });

    // DELETE
    $('#table-to-hire').on('click', '#delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/prestamo/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-to-hire').click();
            }
        });
    });
});