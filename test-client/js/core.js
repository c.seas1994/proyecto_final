$(document).ready(function () {

    $('.tab a').on('click', function (e) {
        
        e.preventDefault();

        $('.tab a').removeClass('active');
        $(this).addClass('active');

        target = $(this).attr('href');
    
        $('.main > div').not(target).hide();
    
        $(target).fadeIn(500);

    });

    $('.tab').click(function(){
        $('.tab').removeClass('active');
        $(this).addClass('active');
    });

});